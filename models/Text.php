<?php


namespace app\models;


use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

class Text extends ActiveRecord
{
    public function rules()
    {
        return [
            [['text'], 'required'],
        ];
    }

    private static function getRandomTextsByCount($textsCount)
    {
        return self::find()->orderBy('RAND()')->limit($textsCount)->all();
    }

    public static function getAllTexts()
    {
        return self::find()->asArray()->all();
    }

    public static function deleteTextById($id)
    {
        $model = self::findOne(['id' => $id]);
        try {
            $model->delete();
        } catch (StaleObjectException $e) {
            return die($e);
        } catch (\Throwable $e) {
            return die($e);
        }

        return true;
    }

    public function createNewText($data)
    {
        $this->text = $data['text'];
        self::save();
    }

    public static function generateNewText($sentences, $textsCount)
    {
        $textsOriginals = self::getRandomTextsByCount($textsCount);
        $textGenerated = '';

        $replacedSymbols = array('.', '-', ',', ':', ';', '(', ')');
        $replaceTo = ' ';

        $handledText = '';

        foreach ($textsOriginals as $text) {
            $text = $text['text'];
            $text = str_replace($replacedSymbols, $replaceTo, $text);
            $handledText = $handledText . ' ' . $text;
        }

        $handledText = explode(' ', $handledText);
        $handledText = array_diff($handledText, [""]);

        for ($sentenceCounter = 1; $sentenceCounter <= $sentences; $sentenceCounter++) {
            $wordsCount = rand(3, 24);
            $sentence = '';

            for ($word = 0; $word <= $wordsCount; $word++) {
                if ($word === 0)
                    $sentence = $sentence . ' ' . mb_convert_case(trim(mb_strtolower($handledText[array_rand($handledText)])), MB_CASE_TITLE, 'UTF-8');
                else
                    $sentence = $sentence . ' ' . trim(mb_strtolower($handledText[array_rand($handledText)]));
            }

            $textGenerated = $textGenerated . $sentence . '. ';
        }

        return $textGenerated;
    }
}