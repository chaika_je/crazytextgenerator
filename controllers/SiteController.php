<?php

namespace app\controllers;

use app\models\Text;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $data = null;

        if(isset($_POST['generate']))
        {
            $data = Text::generateNewText($_POST['sentences'], $_POST['textCount']);
        }

        return $this->render('index', [
            'data' => $data
        ]);
    }


    /**
     * Displays add text page.
     *
     * @return string
     */
    public function actionAddText()
    {
        $model = new Text();

        if (Yii::$app->request->post()) {
            $model->createNewText(Yii::$app->request->post('Text'));
            return $this->goHome();
        }

        return $this->render('addText', [
            'model' => $model,
        ]);
    }


    /**
     * Displays all texts as list page.
     *
     * @return string
     */
    public function actionTextList()
    {
        return $this->render('textList', [
            'data' => Text::getAllTexts(),
        ]);
    }

    public function actionDeleteText()
    {
        try {
            $id = Yii::$app->request->get('id');
            Text::deleteTextById($id);
            return $this->redirect('text-list');
        } catch (\Exception $exception) {
            return die($exception);
        }

    }
}
