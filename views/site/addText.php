<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin(['id' => 'contact-form']);

?>

<?= $form->field($model, 'text')->textarea(['rows' => 6])->label('Добавьте текст')?>

    <div class="form-group">
        <?= Html::submitButton('Поехали!', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
    </div>

<?php ActiveForm::end(); ?>