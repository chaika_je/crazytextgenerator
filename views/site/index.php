<?php

/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title = 'Бредогенератор';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Бредогенератор</h1>

        <p class="lead">Для того что-бы получить порцию бреда, нажмите на зеленую кнопку ниже</p>

        <form id="main-form" method="post" action="/">
            <div class="form-group">
                <p>
                    <button type="submit" name="generate" class="btn btn-lg btn-success" href="">Начать бредить!</button>
                </p>
                <div class="row">
                    <p class="col-md-6">
                        <label for="sel1">Колличество исходных текстов</label>
                        <select class="form-control" id="sel1" name="textCount">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3" selected>3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </p>
                    <p class="col-md-6">
                        <label for="sel2">Колличество предложений:</label>
                        <select class="form-control" id="sel2" name="sentences">
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7" selected>7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </p>
                </div>
            </div>
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        </form>
    </div>

    <div class="body-content">
        <hr>
        <p><?= $data; ?></p>
    </div>
</div>
