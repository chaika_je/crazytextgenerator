<?php
//echo "<pre>";
//var_dump($data);
//die();
?>

<?php foreach ($data as $text): ?>
<div class="panel panel-default">
    <div class="panel-body">
        <?= $text['text']; ?>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-6">
                <p class="text-created-at">
                    Добавлено: <?= $text['created'] ?>
                </p>
            </div>
            <div class="col-md-6">
                <a href="/delete-text?id=<?= $text['id'] ?>" class="btn btn-danger delete-text-button">Удалить</a>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>